
/////////////////////////////////////////////////////////
//
// Polyfills
//
/////////////////////////////////////////////////////////

// replaces Array.from(node_list);
ArrayFrom = function (list) {
  var array = [];
  for (i = 0; i < list.length; i++) {
    array.push(list[i]);
  }
  return array;
};

function getWindowScale() {
  if (window.innerHeight != undefined) {
    return ({
      width: window.innerWidth,
      height: window.innerHeight
    })
  } else if (document.documentElement.clientWidth != undefined) {
    return ({
      width: document.documentElement.clientHeight,
      height: document.documentElement.clientWidth
    })
  } else {
    console.log("You should probably update your browser. Older browsers are a security risk.");
  }
}

function getWindowScroll() {
  if (window.scrollY != undefined) {
    return ({
      x: window.scrollX,
      y: window.scrollY
    })
  } else if (window.pageXOffset != undefined) {
    return ({
      x: window.pageXOffset,
      y: window.pageYOffset
    })
  }
}




// this is the animation for the loan score dial and counter.
function showCustomAnimation(_delay) {
  var container = document.querySelector('.score-content');
  container.savingsText = container.querySelector('.savings .lrg');
  container.scoreText = container.querySelector('.score .lrg');
  container.scoreDial = container.querySelector('.score .dial');

  var savText = container.savingsText.innerText;
  // console.log(savText, savText.replace(',', ''));

  savText = savText.replace(',', '');

  var obj = {
    savings: savText,
    score: parseInt(container.scoreText.innerText)
  }
  TweenMax.from(obj, 1, {
    savings: 0, score: 0, ease: Power3.easeInOut, delay: _delay, onUpdate: function () {
      var sav = Math.round(obj.savings);
      if (sav > 999) {
        sav = sav + "";
        sav = sav.substring(0, 1) + ',' + sav.substring(1, 4)
      }
      var sco = Math.round(obj.score);
      container.savingsText.innerText = sav;
      container.scoreText.innerText = sco;
    }
  });

  TweenMax.from(container.scoreDial, 1.25, { rotation: "-=280", ease: Power3.easeInOut, delay: _delay });
}

// Scroll loader 
function scrollLoader() {
  var elements = ArrayFrom(document.querySelectorAll('.scoll-loader-container'));
  // console.log('window.scrollY: ', getWindowScroll().y);

  var pageBottom = getWindowScroll().y + getWindowScale().height;
  var padding = -100;
  var delay = 0.3;
  elements.map(function (element) {
    // console.log('>>> element', element, element.offsetTop, pageBottom);
    if (!element.classList.contains('showing') && element.offsetTop < pageBottom + padding) {
      element.style.transitionDelay = delay + 's';
      element.classList.add('showing');
      var children = ArrayFrom(element.querySelectorAll('.scoll-loader-element'));
      children.map(function (child) {
        child.style.transitionDelay = delay + 's';
        child.classList.add('showing');

        // conditions for custom classes
        if (child.classList.contains('custom')) {
          showCustomAnimation(delay);
        }

        delay += 0.05;
      })
      // if element has scrolled past the current viewport, reset delay:
      // console.log('---', element.offsetTop + element.offsetHeight, element.offsetTop, element.offsetHeight, getWindowScroll().y);

      if (element.offsetTop + element.offsetHeight < getWindowScroll().y) {
        delay = 0.3;
      } else {
        delay += 0.1;
      }
    }
  });

  // clear interval is all containers are showing
  if (elements.length === document.querySelectorAll('.scoll-loader-container.showing').length) {
    window.clearInterval(scrollLoaderInterval);
  }
}



function resizeScoreGraphic() {
  var container = document.querySelector('.score-content');
  var parent = document.querySelector('.what-is-loanscore .col.custom');
  var imgWidth = 415;

  var scale = parent.offsetWidth / imgWidth;
  if (scale > 1) scale = 1;

  // console.log("scale: ", scale);

  container.style.transform = "translateX(-50%) scale(" + scale + ")";


}



function init() {
  scrollLoaderInterval = window.setInterval(scrollLoader, 50);
  // scrollLoaderInterval = window.setTimeout(scrollLoader, 50);
  window.addEventListener('resize', resizeScoreGraphic)
  resizeScoreGraphic();
}

var scrollLoaderInterval;
window.addEventListener('load', function () {
  init();
})