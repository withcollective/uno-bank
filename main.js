(function () {
  $('h3', '.acc').click(function () {
    $('.acc-content', $(this).parent().parent()).slideUp(250);
    $('.acc-item').removeClass('open'); // OPTIONAL, comment out if multiple open cards are required
    var $content = $('.acc-content', $(this).parent());
    if (!$content.is(':visible')) {
      $content.slideDown(250);
      $(this).parent().addClass('open');
    }
  });

  function checkStickyPos() {
    var pos = $('#sticky_trigger').offset().top;
    var pos2 = $('#hide_sticky').offset().top;

    if ($(window).scrollTop() + 80 >= pos && $(window).scrollTop() + $(window).innerHeight() <= pos2) {
      $('.sticky').addClass('show');
    } else {
      $('.sticky').removeClass('show');
    }
  }

  $(window).scroll(function () {
    checkStickyPos();
  });

  checkStickyPos();
})();

